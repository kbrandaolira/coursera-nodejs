const express = require("express");
const bodyParser = require("body-parser");

const leaderRouter = express.Router();

leaderRouter.use(bodyParser.json());

leaderRouter
  .route("/")
  .all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
  })
  .get((req, res, next) => {
    res.end("Will send all the leaders to you!");
  })
  .post((req, res, next) => {
    res.end("Will add the leader: " + req.body.name);
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end("PUT operation not supported on /leaders");
  })
  .delete((req, res, next) => {
    res.end("Deleting all leaders");
  });

leaderRouter
  .route("/:id")
  .all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
  })
  .get((req, res, next) => {
    res.end("Will send leader with id " + req.params.id);
  })
  .post((req, res, next) => {
    res.end("Will save leader with id " + req.params.id);
  })
  .put((req, res, next) => {
    res.end("Will update leader with id " + req.params.id);
  })
  .delete((req, res, next) => {
    res.end("Will delete leader with id " + req.params.id);
  });

module.exports = leaderRouter;
