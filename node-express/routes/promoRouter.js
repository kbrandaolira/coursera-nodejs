const express = require("express");
const bodyParser = require("body-parser");

const promoRouter = express.Router();

promoRouter.use(bodyParser.json());

promoRouter
  .route("/")
  .all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
  })
  .get((req, res, next) => {
    res.end("Will send all the promotions to you!");
  })
  .post((req, res, next) => {
    res.end("Will add the promotion: " + req.body.name);
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end("PUT operation not supported on /promotions");
  })
  .delete((req, res, next) => {
    res.end("Deleting all promotions");
  });

promoRouter
  .route("/:id")
  .all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
  })
  .get((req, res, next) => {
    res.end("Will send promotion with id " + req.params.id);
  })
  .post((req, res, next) => {
    res.end("Will save promotion with id " + req.params.id);
  })
  .put((req, res, next) => {
    res.end("Will update promotion with id " + req.params.id);
  })
  .delete((req, res, next) => {
    res.end("Will delete promotion with id " + req.params.id);
  });

module.exports = promoRouter;
